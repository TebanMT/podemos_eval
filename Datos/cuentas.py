"""
Creado en Dic 29, 2018

@author: Esteban Mendiola"""
from Podemos_Eval.Datos.conexion import *
 
class Cuentas(Base):
    """Clase que contiene el modelo(tabla) de la base de datos: cuentas
        relacionado con la tabla grupos, calendariopagos y transacciones"""

    __tablename__ = "cuentas"
 
    id = Column(String(2), primary_key=True)
    grupo_id = Column(String(5), ForeignKey('grupos.id'))
    estatus = Column(String(15))
    monto = Column(Numeric(15,2))
    saldo = Column(Numeric(15,2))
    grupo = relationship("Grupos", backref=backref("cuentas"))
    calendario_pago = relationship("Calendario", backref=backref("cuentas"))
    transaccion = relationship("Transacciones", backref=backref("cuentas"))
    

    def __init__(self, id, grupo_id, estatus, monto, saldo):
        """Cosntructor"""
        self.id = id
        self.grupo_id = grupo_id
        self.estatus = estatus
        self.monto = monto
        self.saldo = saldo
