"""
Creado en Dic 29, 2018

@author: Esteban Mendiola"""
from Podemos_Eval.Datos.conexion import *
 
class Miembros(Base):
    """Clase que contiene el modelo(tabla) de la base de datos: miembros
        relacionado con la tabla clientes y grupos
        Nota: Esta tabla es creada mendiate la relacion m:m entre clientes y grupos,
        al no contener mas campos que las claves primarias de las tablas clientes y grupos 
        se podria omitir con una estructura parecida a la siguiente:

                association_table = Table('association', Base.metadata,
                Column('left_id', Integer, ForeignKey('left.id')),
                Column('right_id', Integer, ForeignKey('right.id'))"""

    __tablename__ = "miembros"
 
    grupo_id = Column(String(5), ForeignKey('grupos.id'), primary_key=True)
    cliente_id = Column(String(7), ForeignKey('clientes.id'))
    cliente = relationship("Clientes", backref=backref("miembros"))
    grupo = relationship("Grupos", backref=backref("miembros"))
    
 
    def __init__(self, grupo_id, cliente_id):
        """Constructor"""
        self.grupo_id = grupo_id
        self.cliente_id = cliente_id
