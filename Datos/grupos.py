"""
Creado en Dic 29, 2018

@author: Esteban Mendiola"""
from Podemos_Eval.Datos.conexion import *
 
class Grupos(Base):
    """Clase que contiene el modelo(tabla) de la base de datos: grupos
        relacionado con la tabla clientes(mediate la relacion miembros) y grupos"""
    
    __tablename__ = "grupos"
 
    id = Column(String(5), primary_key=True)
    nombre = Column(String(20))
    cliente = relationship("Clientes", secondary="miembros")
    cuenta = relationship("Cuentas", backref=backref("grupos"))
 
    def __init__(self, id, nombre):
        """Constructor"""
        self.id = id
        self.nombre = nombre
