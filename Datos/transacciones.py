"""
Creado en Dic 29, 2018

@author: Esteban Mendiola"""
from Podemos_Eval.Datos.conexion import *
 
class Transacciones(Base):
    """Clase que contiene el modelo(tabla) de la base de datos: transacciones
        relacionado con la tabla cuentas """

    __tablename__ = "transacciones"
 
    id = Column(Integer, primary_key=True)
    cuenta_id = Column(String(5), ForeignKey('cuentas.id'))
    fecha = Column(DateTime)
    monto = Column(Numeric(15,2))
    cuenta = relationship("Cuentas", backref=backref("transacciones"))
 
    #----------------------------------------------------------------------
    def __init__(self, id, cuenta_id, fecha, monto):
        """Cosntructor"""
        self.id = id
        self.cuenta_id = cuenta_id
        self.fecha = fecha
        self.monto = monto
