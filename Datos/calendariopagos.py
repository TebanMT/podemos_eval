"""
Creado en Dic 28, 2018

@author: Esteban Mendiola"""
from Podemos_Eval.Datos.conexion import *
 
########################################################################
class Calendario(Base):
    """Clase que contiene el modelo(tabla) de la base de datos: calendariopagos
        relacionado con la tabla cuenta"""

    __tablename__ = "calendariopagos"
 
    id = Column(Integer, primary_key=True)
    cuenta_id = Column(String(5), ForeignKey('cuentas.id'))
    num_pago = Column(Integer)
    monto = Column(Numeric(15,2))
    fecha_pago = Column(Date)
    estatus = Column(String(15))
    cuenta = relationship("Cuentas",  backref=backref("calendariopagos"))
 
    #----------------------------------------------------------------------
    def __init__(self, id, cuenta_id, num_pago, fecha_pago, estatus):
        """Constructor"""
        self.id = id
        self.cuenta_id = cuenta_id
        self.num_pago = num_pago
        self.fecha_pago = fecha_pago
        self.estatus = estatus
