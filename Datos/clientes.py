"""
Creado en Dic 28, 2018

@author: Esteban Mendiola"""

from Podemos_Eval.Datos.conexion import *
 
########################################################################
class Clientes(Base):
    """Clase que contiene el modelo(tabla) de la base de datos: clientes
        relacionado con la tabla grupos"""

    __tablename__ = "clientes"
 
    id = Column(String(7), primary_key=True)
    nombre = Column(String(60))
    grupo = relationship("Grupos", secondary="miembros")

    #----------------------------------------------------------------------
    def __init__(self, id, nombre):
        """Constructor"""
        self.id = id
        self.nombre = nombre


