# -*- coding: utf-8 -*-
"""
Creado en Dic 30, 2018

@author: Esteban Mendiola

Modulo que implementa la logica del programa """

from Podemos_Eval.Datos import session
from Podemos_Eval.Datos.clientes import Clientes 
from Podemos_Eval.Datos.grupos import Grupos
from Podemos_Eval.Datos.miembros import Miembros
from Podemos_Eval.Datos.calendariopagos import Calendario
from Podemos_Eval.Datos.transacciones import Transacciones
from Podemos_Eval.Datos.cuentas import Cuentas
from decimal import Decimal, InvalidOperation
import doctest

    
def mostrarClientes():
    """Funcion para obtener el listado de todos los clientes almacenados en la BD
        Valor de retorno: Lista
        Estructura de la Lista: 
                                [Object.Cliente,,,n]"""
        
    lista_clientes = []
    clientes = session.query(Clientes).all()
    for cliente in clientes:
        lista_clientes.append(cliente)
    return lista_clientes

def mostrarGrupos():
    """Funcion para obtener el listado de todos los grupos existentesn en la BD, incluye el nombre de cada integrante
        Valor de retorno: Diccionario
        Estructura del Diccionario:
                                    {Nombre_grupo: [Nombre_cliente,,,n],,,,,,n}"""
    diccionario_grupos = {}
    grupos = session.query(Grupos).all()
    for grupo in grupos:
        lista_aux = []
        for cliente in grupo.cliente:
            lista_aux.append(cliente.nombre)
        diccionario_grupos[grupo.nombre] = lista_aux
        
    return diccionario_grupos

def mostrarCalendario(cuenta):
    """Funcion para obtener el calendario de pagos de una cuenta dada
       Valor de retorno: Lista 
       Estructura de la Lista: 
                                [Object.Calendario,,,n]"""

    calendarios = session.query(Calendario).filter_by(cuenta_id=cuenta)
    lista_calendario = []
    for calendario in calendarios:
        lista_calendario.append(calendario)
    return lista_calendario 

def mostrarTransacciones(cuenta):
    """Funcion para obtener las transacciones de una cuenta dada
       Valor de retorno: Lista 
       Estructura de la Lista: 
                                [Object.Transacciones,,,n]"""

    transacciones = session.query(Transacciones).filter_by(cuenta_id=cuenta)
    lista_transacciones = []
    for transaccion in transacciones:
        lista_transacciones.append(transaccion)
    return lista_transacciones 

def mostrarCuentas():
    """Funcion para obtener las cuentas de cada grupo
       Valor de retorno: Diccionario 
       Estructura del Diccionario:
                                    {Nombre_grupo: [Object.Cuenta,,,n],,,,n}"""
    
    diccionario_cuentas = {}
    grupos = session.query(Grupos).all()
    for grupo in grupos:
        lista_aux = []
        for cuenta in grupo.cuenta:
            lista_aux.append(cuenta)
        diccionario_cuentas[grupo.nombre] = lista_aux
    return diccionario_cuentas

def pagarCuenta(monto, cuenta):
    """Funcion para agregar un pago a la cuenta seleccionado, si el pago excede el monto de la cuenta 
    este es rechazado
    Valor de retorno: String"""
    
    cuenta=session.query(Cuentas).filter_by(id=cuenta).first()
    mensaje = ""
    try:
        monto = Decimal(monto)
        if monto > cuenta.saldo:
            mensaje = "El pago ecxede el saldo de la cuenta"
        else:
            cuenta.saldo -= monto
            session.commit()
            if cuenta.saldo==0:
                cuenta.estatus = "CERRADA"
                session.commit()
            mensaje = "Pago agregado Correctamente."
    except InvalidOperation as exc:
        mensaje = "Error de conversion, por favor ingrese unicamente numeros"
    return mensaje
