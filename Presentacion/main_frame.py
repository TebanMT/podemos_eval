# -*- coding: utf-8 -*-
"""
Creado en Ene 01, 2019

@author: Esteban Mendiola
"""

from tkinter import ttk, Frame, Tk, N, S, W, E, Button, FLAT, Label,Entry, YES, Toplevel
from Podemos_Eval.Negocio.negocio import *

class App(Frame):
    """Clase que crear un Frame de la libreria Tkinter para la ejecucion de la logica de negocios""" 

    def __init__(self, parent):
        """Constructor"""

        Frame.__init__(self, parent)
        self.parent=parent
        self.initialize_user_interface()

    def initialize_user_interface(self):
        """Dibuja una interfaz de usuario que permite al usuario mostrar clientes, mostrar grupos y 
            mostrar cuentas. Obteniendo los datos de la BD"""
        self.parent.title("Evaluacion Podemos")       
        self.parent.grid_rowconfigure(0,weight=1)
        self.parent.grid_columnconfigure(0,weight=1)
        self.parent.config(background="lavender")


        # Define los elementos de la interface
        self.mostrar_clientes = Button(self.parent,text="Mostar Clientes",relief=FLAT, command=self.vistaMostrarClientes)
        self.mostrar_clientes.grid(row=0,column=0, sticky = W)
        self.mostrar_grupos = Button(self.parent,text="Mostar Grupos",relief=FLAT, command=self.vistaMostrarGrupos)
        self.mostrar_grupos.grid(row=0,column=1, sticky = W)
        self.mostrar_grupos = Button(self.parent,text="Agregar Pago",relief=FLAT, command=self.vistaMostrarCuentasPago)
        self.mostrar_grupos.grid(row=1,column=0, sticky = W)
        self.mostrar_grupos = Button(self.parent,text="Mostrar Cuentas",relief=FLAT, command=self.vistaMostrarCuentas)
        self.mostrar_grupos.grid(row=1,column=1, sticky = W)

        # coloca el treeview vacio
        self.tree = ttk.Treeview( self.parent, columns=('', ''))
        self.tree.grid(row=4, columnspan=4, sticky='nsew')
        self.treeview = self.tree
 
    
    def dobleClickPago(self, event):
        """Emerje una ventada, para agregar un pago a la cuenta seleccionada,
         al dar doble click a un elemento del treeview"""

        item = self.tree.selection()[0]
        self.valores = self.tree.item(item,"values")
        self.ventana_pago = Toplevel(self)
        self.label_enc = Label(self.ventana_pago, text="Agregar Pago a la Cuenta: "+self.valores[0], height=0, width=40) 
        self.label_enc.pack() 
        self.label_text = Label(self.ventana_pago, text="Monto: ") 
        self.label_text.pack() 
        self.monto = Entry(self.ventana_pago) 
        self.monto.pack() 
        self.enviar = Button(self.ventana_pago,text="Pagar",relief=FLAT, command=self.agregarPago)
        self.enviar.pack()
    
    def dobleClickInfo(self, event):
        """Emergen dos ventadas, una con la informacion del calendario de pago y 
        otra con las transacciones"""
        self.calendarioPago()
        self.transacciones()

    def transacciones(self):
        """Crea la ventana donde se muestran los datos de las transacciones de pagos de
        una cuenta selecionada"""

        item = self.tree.selection()[0]
        self.valores = self.tree.item(item,"values")
        self.ventana_trans = Toplevel(self)
        self.ventana_trans.title("Transacciones. Cuenta: "+self.valores[0])
        self.ventana_trans = ttk.Treeview( self.ventana_trans, columns=('', ''))
        self.ventana_trans.grid(row=4, columnspan=4, sticky='nsew')
        self.ventana_trans['columns'] = ('fecha', 'monto')
        self.ventana_trans.heading("#0", text='#')
        self.ventana_trans.column("#0", width=35)
        self.ventana_trans.heading('fecha', text='Fecha')
        self.ventana_trans.column('fecha', stretch=YES)
        self.ventana_trans.heading('monto', text='Monto')
        self.ventana_trans.column('monto', stretch=YES)
        transacciones = mostrarTransacciones(self.valores[0])
        i = 1
        for transaccion in transacciones:
            self.ventana_trans.insert('', 'end', text=i, values=(transaccion.fecha,transaccion.monto))
            i=i+1


    def calendarioPago(self):
        """Crea la ventana donde se muestran los datos del calendario de pagos de
        una cuenta selecionada"""

        item = self.tree.selection()[0]
        self.valores = self.tree.item(item,"values")
        self.ventana_calendario = Toplevel(self)
        self.ventana_calendario.title("Calendario de pagos. Cuenta: "+self.valores[0])
        self.tree_calendario = ttk.Treeview( self.ventana_calendario, columns=('', ''))
        self.tree_calendario.grid(row=4, columnspan=4, sticky='nsew')
        self.tree_calendario['columns'] = ('num','fecha', 'monto', 'estatus')
        self.tree_calendario.heading("#0", text='#')
        self.tree_calendario.column("#0", width=35)
        self.tree_calendario.heading('num', text='Num Pago')
        self.tree_calendario.column('num', stretch=YES)
        self.tree_calendario.heading('fecha', text='Fecha')
        self.tree_calendario.column('fecha', stretch=YES)
        self.tree_calendario.heading('monto', text='Monto')
        self.tree_calendario.column('monto', stretch=YES)
        self.tree_calendario.heading('estatus', text='Estatus')
        self.tree_calendario.column('estatus', stretch=YES)
        calendarios = mostrarCalendario(self.valores[0])
        i = 1
        for calendario in calendarios:
            self.tree_calendario.insert('', 'end', text=i, values=(calendario.num_pago,
                                                            calendario.fecha_pago,
                                                            calendario.monto, calendario.estatus))
            i=i+1

    def agregarPago(self):
        """Enivia el pago a la logica de negocio para que lo porcese y crea una 
        ventana emerjente con el resultado de la llamada a la funcion pagarCuenta almacenado 
        en la carpeta Negocio"""

        ventana_mensaje = Toplevel(self)
        label_enc = Label(ventana_mensaje, text=pagarCuenta(self.monto.get(), self.valores[0]), height=2, width=50) 
        label_enc.pack() 
        self.vistaMostrarCuentas()
        
    
    def vistaMostrarClientes(self):
        """Agrega a el treeview los resultados de la funcion mostrarClientes almacenado
        en la logica de negocio (Carpeta: Negocio)"""

        self.treeview.delete(*self.treeview.get_children()) 
        self.treeview['columns'] = ('id', 'nombre')
        self.treeview.heading("#0", text='#')
        self.treeview.column("#0", width=35)
        self.treeview.heading('id', text='ID')
        self.treeview.column('id', stretch=YES)
        self.treeview.heading('nombre', text='Nombre')
        self.treeview.column('nombre', stretch=YES)
        clientes = mostrarClientes()
        i = 1
        for cliente in clientes:
            self.treeview.insert('', 'end', text=i, values=(cliente.id,cliente.nombre))
            i=i+1
    
    def vistaMostrarGrupos(self):
        """Agrega a el treeview los resultados de la funcion mostrarGrupos almacenado
        en la logica de negocio (Carpeta: Negocio)"""

        self.treeview.delete(*self.treeview.get_children()) 
        self.treeview['columns'] = ('Grupo', 'nombre')
        self.treeview.heading("#0", text='#')
        self.treeview.column("#0", width=35)
        self.treeview.heading('Grupo', text='Grupo')
        self.treeview.column('Grupo', stretch=YES)
        self.treeview.heading('nombre', text='Nombre Cliente')
        self.treeview.column('nombre', stretch=YES)
        grupos = mostrarGrupos()
        i = 1
        for (k,v) in grupos.items():
            for val in v:
                self.treeview.insert('', 'end', text=i, values=(k,val))          
            i=i+1 

    def vistaMostrarCuentasPago(self):
        """Agrega a el treeview los resultados de la funcion mostrarCuentas almacenado
        en la logica de negocio (Carpeta: Negocio) para posteriormete poder agregar 
        pagos con doble click sobre alguna cuenta"""

        self.treeview.delete(*self.treeview.get_children()) 
        self.treeview['columns'] = ('Cuenta','Monto','Saldo','Estatus', 'Grupo', "Accion")
        self.treeview.heading("#0", text='#')
        self.treeview.column("#0", width=35)
        self.treeview.heading('Cuenta', text='Cuenta')
        self.treeview.column('Cuenta', stretch=YES)
        self.treeview.heading('Monto', text='Cuenta')
        self.treeview.column('Monto', stretch=YES)
        self.treeview.heading('Saldo', text='Saldo')
        self.treeview.column('Saldo', stretch=YES)
        self.treeview.heading('Estatus', text='Estatus')
        self.treeview.column('Estatus', stretch=YES)
        self.treeview.heading('Grupo', text='Grupo')
        self.treeview.column('Grupo', stretch=YES)
        self.treeview.heading('Accion', text='Accion')
        self.treeview.column('Accion', stretch=YES)
        self.treeview.bind("<Double-1>", self.dobleClickPago)
        cuentas = mostrarCuentas()
        i = 1
        for (k,v) in cuentas.items():
            for val in v:
                self.treeview.insert('', 'end', text=i, values=(val.id,val.monto,val.saldo,val.estatus,k,"Agregar Pago (Doble Click)"))         
                i=i+1 

    def vistaMostrarCuentas(self):
        """Agrega a el treeview los resultados de la funcion mostrarCuentas almacenado
        en la logica de negocio (Carpeta: Negocio) para posteriormete
        ver su informacion con doble click sobre alguna cuenta"""

        self.treeview.delete(*self.treeview.get_children()) 
        self.treeview['columns'] = ('Cuenta','Monto','Saldo','Estatus', 'Grupo', "Accion")
        self.treeview.heading("#0", text='#')
        self.treeview.column("#0", width=35)
        self.treeview.heading('Cuenta', text='Cuenta')
        self.treeview.column('Cuenta', stretch=YES)
        self.treeview.heading('Monto', text='Cuenta')
        self.treeview.column('Monto', stretch=YES)
        self.treeview.heading('Saldo', text='Saldo')
        self.treeview.column('Saldo', stretch=YES)
        self.treeview.heading('Estatus', text='Estatus')
        self.treeview.column('Estatus', stretch=YES)
        self.treeview.heading('Grupo', text='Grupo')
        self.treeview.column('Grupo', stretch=YES)
        self.treeview.heading('Accion', text='Accion')
        self.treeview.column('Accion', stretch=YES)
        self.treeview.bind("<Double-1>", self.dobleClickInfo)
        cuentas = mostrarCuentas()
        i = 1
        for (k,v) in cuentas.items():
            for val in v:
                self.treeview.insert('', 'end', text=i, values=(val.id,val.monto,val.saldo,val.estatus,k,"Info(Doble Click)"))         
                i=i+1       

