Evaluacion para el Proceso de selecion de la empresa Podemos Progresar

INTRODUCCION

El presente proyecto fue realizado con la finalidad de ser evaluado en el proceso de selecion de la empresa Podemos Progresar, para el puesto de 
Desarrollador Python.

INFORMACION DEL PROYECTO

Para el desarrollo se utilizo: 

                              -Python 3.5.2
                              
                              -Windows 10
                            
                              -virtualenvs 

El proyecto es una aplicacion de escritorio, consta de una arquitectura de 3 capas en un nivel, parecida a la arquitectura MVC, decidi utilizar esta arquitectura
por que permite tener una alta Modularidad.

Se utilizo un ORM (SQLAlchemy) para mapear la BD, en mi opinion esto nos permite tener una mayor Choesion del codigo y un bajo aclopamiento.

Para la parte de la interface se utilizo la libreria Tkinter que viene por defecto en python.

IMPORTACION DE ARCHIVOS

El archivo admi.sql se importo desde la consola con el siguiente comando: mysql -u root < admin.sql

Una vez creado el usuario eval se importo el archivo modelo_script con el siguiente comando: mysql -u eval -p password < modelo_scrip.sql

Para la importacion de los archivos .cv se realizo desde el panel de control de mysql siguiendo los pasos:

    -seleccionar DB
    -seleccionar tabla
    -Importar
    -seleccionar archivo
    -"Omitir esta cantidad de consultas (en SQL) o líneas (en otros formatos) desde la primera:" 1
    -continuar

Para los archivos data_calaendariopago.cv y data_transacciones.cv de modificaron los campos fecha(para que considiera con el tipo de dato en sus tablas
correspondientes) de la siguiente manera:

    -seleccionar campos
    -mas formatos de numero
    -fecha
    -tipo: yyyy-mm-dd (para el archivo data_calendariopagos.cv)
    -tipo: aaaa-mm-dd hh:mm (para el archivo data_transacciones.cv)
    -guardar

INSTALACION

Para que el proyecto pueda ser ejecutado de manera correcta se necesitaran las librerias de SqlAlchemy y PyMySQ, ambas apis 
estan descritas en el documento requerimientos.

ESTRUCTURA DEL PROYECTO

Carpeta Datos: se encuentran todos los modelos mapeados de la BD

Carpeta Negocio: se encuentra la logica que da funcionalidad al proyecto

Carpeta Presentacion: se encuentra el frame tkinter 

Carpeta Prubas Unitarias: se encuentran las pruebas unitarias de las funciones de la logica de negocio 

Archivo app.py: Es es archivo ejecutable 

INFORMACION EXTRA

---------- Para Javier Novoa-----------

Si bien no es una representacion exacta de la arquitectura de 3 capas, trate de acercarme lo mas posible, ya que, sinceramente, implementar 
la arquitectura es de lo que mas me cuesta, pensienso mucho en que arquitectura utilizar y cuando la implemento me surguen las dudas, un 
ejemplo claro en este proyecto fue:
    
    cuando estaba realizando la logica me di cuenta que no era necesario implementar una clase para esta y era posible 
    implementar los metodos de la logica directamente en el modelo, al final no lo realice de esa manera y continue con lo 
    que tenia planeado des de el inicio -quiza sea falta de experiencia-

En cuanto al ORM segui la recomendacion y utilice SqlAlchemy, nunca habia utilizado un ORM en Python.
 
En cuanto a las pruebas unitarias solo realice del modulo de negocio ya que no tuve oportunidad de realizar de todas las funciones.

--------------------------