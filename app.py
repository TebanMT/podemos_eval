__author__ = "Ing. Esteban Mendiola Téllez"
__license__ = "GPL"
__version__ = "1.0"
__maintainer__ = "Esteban Mendiola"
__email__ = "Mendiola_esteban@outlook.com"
__status__ = "Desarrollo"

from Podemos_Eval.Presentacion.main_frame import *

def main():
    root=Tk()
    d=App(root)
    root.mainloop()

if __name__=="__main__":
    main()
